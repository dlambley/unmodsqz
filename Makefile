# Makefile for unmodsqz

COMPONENT ?= unmodsqz
INSTAPP = ${INSTDIR}${SEP}!UnModSqz

OBJS = unsqueezec unmodsqz
LIBS = ${CLXLIB}
INSTAPP_FILES = !Boot !Help !Run !Sprites [!Sprites11] !Sprites22 Desc Messages Templates
INSTAPP_VERSION = Desc

include CApp

# Dynamic dependencies:
